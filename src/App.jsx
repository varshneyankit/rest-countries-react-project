import { useState } from "react";
import { Outlet } from "react-router-dom";

import Navbar from "./components/Navbar";
import ThemeContext from "./utils/ThemeContext";

function App() {
  const [theme, setTheme] = useState({
    themeMode: "light",
  });

  return (
    <ThemeContext.Provider
      value={{
        theme: theme,
        setTheme: setTheme,
      }}
    >
      <div className="app bg-primary text-primary min-h-[100vh]">
        <div className="app-container">
          <Navbar />
          <Outlet />
        </div>
      </div>
    </ThemeContext.Provider>
  );
}

export default App;
