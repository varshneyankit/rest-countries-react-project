export async function fetchAPIData(API_URL) {
  const response = await fetch(API_URL);

  if (response.ok) {
    const json = await response.json();
    return json;
  } else {
    throw new Error("Unable to fetch API data");
  }
}

export function getFilteredCountriesData(
  data,
  selectedRegion,
  selectedSubregion,
  selectedPopulationOrder,
  selectedAreaOrder,
  searchedText
) {
  let filteredData = data;

  if (selectedRegion == "") {
    filteredData = data;
  } else {
    filteredData = filteredData.filter(
      (countryData) =>
        countryData.region.toLowerCase() == selectedRegion.toLowerCase()
    );
  }

  if (selectedRegion != "" && selectedSubregion != "") {
    filteredData = filteredData.filter(
      (countryData) =>
        countryData.subregion.toLowerCase() == selectedSubregion.toLowerCase()
    );
  }

  filteredData = filteredData.filter((countryData) =>
    countryData.name.common.toLowerCase().includes(searchedText.toLowerCase())
  );

  if (selectedPopulationOrder == "increasing") {
    filteredData = filteredData.sort((firstCountryData, secondCountryData) => {
      let firstCountryPopulation = parseInt(firstCountryData.population);
      let secondCountryPopulation = parseInt(secondCountryData.population);

      return firstCountryPopulation > secondCountryPopulation ? 1 : -1;
    });
  } else if (selectedPopulationOrder == "decreasing") {
    filteredData = filteredData.sort((firstCountryData, secondCountryData) => {
      let firstCountryPopulation = parseInt(firstCountryData.population);
      let secondCountryPopulation = parseInt(secondCountryData.population);

      return firstCountryPopulation > secondCountryPopulation ? -1 : 1;
    });
  }

  if (selectedAreaOrder == "increasing") {
    filteredData = filteredData.sort((firstCountryData, secondCountryData) => {
      let firstCountryArea = parseInt(firstCountryData.area);
      let secondCountryArea = parseInt(secondCountryData.area);

      return firstCountryArea > secondCountryArea ? 1 : -1;
    });
  } else if (selectedAreaOrder == "decreasing") {
    filteredData = filteredData.sort((firstCountryData, secondCountryData) => {
      let firstCountryArea = parseInt(firstCountryData.area);
      let secondCountryArea = parseInt(secondCountryData.area);

      return firstCountryArea > secondCountryArea ? -1 : 1;
    });
  }

  return filteredData;
}

export function getRegionsData(data) {
  const regionsData = {};

  data.forEach((countryData) => {
    const currentRegion = countryData.region;
    const currentSubregion = countryData.subregion;

    if (regionsData[currentRegion] == undefined) {
      regionsData[currentRegion] = [];

      if (currentSubregion) {
        regionsData[currentRegion].push(currentSubregion);
      }
    } else if (
      currentSubregion != undefined &&
      regionsData[currentRegion].includes(currentSubregion) == false
    ) {
      regionsData[currentRegion].push(currentSubregion);
    }
  });

  return regionsData;
}
