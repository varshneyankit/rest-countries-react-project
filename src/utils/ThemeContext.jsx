import { createContext } from "react";

const ThemeContext = createContext({
  theme: {
    themeMode: "light",
  },
});

export default ThemeContext;
