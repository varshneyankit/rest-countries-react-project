import { useRouteError, Link } from "react-router-dom";

function ErrorPage() {
  const error = useRouteError();

  return (
    <div className="error-page text-center text-xl font-medium mt-[60px]">
      <h1 className="text-2xl">Oops!</h1>
      <h2>Something went wrong</h2>
      <h2>{error.status + " : " + error.statusText}</h2>
      <Link
        to={"/"}
        className="inline-flex cursor-pointer mt-[20px] px-8 py-2 hover:bg-gray-800 hover:text-white ease-in-out duration-300"
      >
        Go back to Homepage
      </Link>
    </div>
  );
}

export default ErrorPage;
