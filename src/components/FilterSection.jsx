import { SearchIcon } from "../icons";

function FilterSection({
  regionsData,
  searchedText,
  setSearchedText,
  selectedRegion,
  setSelectedRegion,
  setSelectedSubregion,
  selectedPopulationOrder,
  setSelectedPopulationOrder,
  selectedAreaOrder,
  setSelectedAreaOrder,
}) {
  return (
    <div className="filter-section">
      <div className="filter-section-container flex flex-col lg:flex-row justify-between align-middle">
        <div className="filter-search-section">
          <div className="search-container bg-secondary text-primary rounded flex px-6 py-4 mb-8 lg:mb-0 shadow-md">
            <SearchIcon />
            <input
              type="text"
              name="search"
              id="search"
              placeholder="Search for a country..."
              className="bg-secondary text-primary text-md pl-4 outline-none"
              value={searchedText}
              onInput={(e) => {
                setSearchedText(e.target.value);
              }}
            />
          </div>
        </div>
        <div className="filter-sort-section flex flex-col lg:flex-row gap-4 justify-around align-middle">
          <select
            name="sort-by-area"
            id="sort-by-area"
            className="bg-secondary text-primary rounded px-4 py-4 shadow-md outline-none"
            value={selectedAreaOrder}
            onChange={(e) => {
              setSelectedAreaOrder(e.target.value);
              setSelectedPopulationOrder("");
            }}
          >
            <option value="">Filter by Area</option>
            <option value="increasing">Increasing</option>
            <option value="decreasing">Decreasing</option>
          </select>
          <select
            name="sort-by-population"
            id="sort-by-population"
            className="bg-secondary text-primary rounded px-4 py-4 shadow-md outline-none"
            value={selectedPopulationOrder}
            onChange={(e) => {
              setSelectedPopulationOrder(e.target.value);
              setSelectedAreaOrder("");
            }}
          >
            <option value="">Filter by Population</option>
            <option value="increasing">Increasing</option>
            <option value="decreasing">Decreasing</option>
          </select>
          <select
            name="sort-by-region"
            id="sort-by-region"
            className="bg-secondary text-primary rounded px-4 py-4 shadow-md outline-none"
            onChange={(e) => {
              setSelectedRegion(e.target.value);
              setSelectedSubregion("");
            }}
          >
            <option value="">Filter by Region</option>
            {Object.keys(regionsData)
              .sort()
              .map((region, index) => (
                <option key={region + index} value={region}>
                  {region}
                </option>
              ))}
          </select>
          {selectedRegion != "" && (
            <select
              name="sort-by-subregion"
              id="sort-by-subregion"
              className="bg-secondary text-primary rounded px-4 py-4 shadow-md outline-none"
              onChange={(e) => {
                setSelectedSubregion(e.target.value);
              }}
            >
              <option value="">Filter by Subregion</option>
              {regionsData[selectedRegion].map((subregion, index) => (
                <option key={subregion + index} value={subregion}>
                  {subregion}
                </option>
              ))}
            </select>
          )}
        </div>
      </div>
    </div>
  );
}

export default FilterSection;
