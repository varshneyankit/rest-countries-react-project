function CountryCard({ countryData }) {
  let countryName = countryData.name.common;
  let flagURL = countryData.flags.png;
  let flagAltText = countryData.flags.alt;
  let population = countryData.population.toLocaleString();
  let region = countryData.region;
  let capital = undefined;

  if (Array.isArray(countryData?.capital) && countryData.capital.length > 0) {
    capital = countryData.capital[0];
  }

  return (
    <div className="country-card bg-secondary w-[250px] shadow-md">
      <div className="card-container rounded">
        <div className="card-image-container h-[160px] rounded">
          <img
            className="h-[100%] w-[100%] rounded"
            src={flagURL}
            alt={flagAltText}
          />
        </div>
        <div className="card-data-container p-[20px] rounded pb-[30px]">
          <h2 className="country-name font-semibold mb-4 text-xl">
            {countryName}
          </h2>
          <p className="population mb-1">
            <span className="font-bold">Population: </span>
            <span>{population}</span>
          </p>
          <p className="region mb-1">
            <span className="font-bold">Region: </span> <span>{region}</span>
          </p>
          <p className="capital mb-1">
            <span className="font-bold">Capital: </span> <span>{capital}</span>
          </p>
        </div>
      </div>
    </div>
  );
}

export default CountryCard;
