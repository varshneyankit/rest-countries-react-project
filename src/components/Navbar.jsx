import { useContext } from "react";
import ThemeContext from "../utils/ThemeContext";

import { ThemeIcon } from "../icons";

function Navbar() {
  const { theme, setTheme } = useContext(ThemeContext);

  return (
    <nav className="navbar bg-secondary shadow-md px-4">
      <div className="navbar-container flex justify-between align-middle py-6 max-w-[1200px] mx-auto">
        <h1 className="logo-text font-bold text-lg lg:text-2xl">
          Where in the world?
        </h1>
        <button
          className="dark-btn flex gap-2 justify-between align-middle"
          onClick={() => {
            setTheme({
              ...theme,
              themeMode: theme.themeMode == "light" ? "dark" : "light",
            });
            document.documentElement.classList.toggle("light");
            document.documentElement.classList.toggle("dark");
          }}
        >
          <ThemeIcon />
          <span className="flex">Dark Mode</span>
        </button>
      </div>
    </nav>
  );
}

export default Navbar;
