import { useState, useEffect } from "react";

import FilterSection from "./FilterSection";
import CountrySection from "./CountrySection";
import {
  fetchAPIData,
  getFilteredCountriesData,
  getRegionsData,
} from "../utils/helper";

function Homepage() {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);
  const [searchedText, setSearchedText] = useState("");
  const [selectedRegion, setSelectedRegion] = useState("");
  const [selectedSubregion, setSelectedSubregion] = useState("");
  const [selectedPopulationOrder, setSelectedPopulationOrder] = useState("");
  const [selectedAreaOrder, setSelectedAreaOrder] = useState("");

  const API_URL = "https://restcountries.com/v3.1/all";
  const filteredData = getFilteredCountriesData(
    data,
    selectedRegion,
    selectedSubregion,
    selectedPopulationOrder,
    selectedAreaOrder,
    searchedText
  );
  const regionsData = getRegionsData(data);

  useEffect(() => {
    fetchAPIData(API_URL)
      .then((json) => {
        setData(json);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  return (
    <div className="homepage px-4 mt-[30px]">
      {error ? (
        <div>
          <p className="text-lg text-center">{error.message}</p>
        </div>
      ) : data.length == 0 ? (
        <div className="loading-page">
          <div className="loading-page-container flex justify-center mt-[180px]">
            <img src="./loading.gif" alt="Loading GIF" />
          </div>
        </div>
      ) : (
        <div className="homepage-container max-w-[1200px] mx-auto">
          <FilterSection
            regionsData={regionsData}
            searchedText={searchedText}
            setSearchedText={setSearchedText}
            selectedRegion={selectedRegion}
            setSelectedRegion={setSelectedRegion}
            setSelectedSubregion={setSelectedSubregion}
            selectedPopulationOrder={selectedPopulationOrder}
            setSelectedPopulationOrder={setSelectedPopulationOrder}
            selectedAreaOrder={selectedAreaOrder}
            setSelectedAreaOrder={setSelectedAreaOrder}
          />
          <CountrySection data={filteredData} />
        </div>
      )}
    </div>
  );
}

export default Homepage;
