import { Link } from "react-router-dom";

import CountryCard from "./CountryCard";

function CountrySection({ data }) {
  return (
    <div className="country-section mt-[30px]">
      <div className="country-section-container">
        <div className="country-cards-section flex flex-wrap gap-12 justify-around">
          {data.length == 0 ? (
            <div className="no-data-section mt-[80px]">
              <p className="text-lg">No filtered data is available!</p>
            </div>
          ) : (
            data.map((countryData) => {
              return (
                <Link
                  to={"/country/" + countryData.cca3}
                  key={countryData.cca3}
                >
                  <CountryCard countryData={countryData} />
                </Link>
              );
            })
          )}
        </div>
      </div>
    </div>
  );
}

export default CountrySection;
