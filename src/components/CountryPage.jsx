import { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";

import { fetchAPIData } from "../utils/helper";
import { BackIcon } from "../icons";

function CountryPage() {
  const [data, setData] = useState([]);
  const [error, setError] = useState(null);

  const API_URL = "https://restcountries.com/v3.1/all";
  const { id } = useParams();

  useEffect(() => {
    fetchAPIData(API_URL)
      .then((json) => {
        setData(json);
      })
      .catch((error) => {
        setError(error);
      });
  }, []);

  const countryData = data.find((countryData) => countryData.cca3 == id);

  let countryName = countryData?.name.common;
  let flagURL = countryData?.flags.png;
  let flagAltText = countryData?.flags.alt;
  let population = countryData?.population.toLocaleString();
  let region = countryData?.region;
  let subregion = countryData?.subregion;
  let borderCodes = countryData?.borders;
  let nativeName = undefined;
  let capital = undefined;
  let topLevelDomain = undefined;
  let currencies = undefined;
  let languages = undefined;

  if (countryData?.name?.nativeName) {
    let nativeNamesData = Object.values(countryData?.name?.nativeName);
    nativeName = nativeNamesData[nativeNamesData.length - 1].common;
  }

  if (Array.isArray(countryData?.capital) && countryData.capital.length > 0) {
    capital = countryData.capital[0];
  }

  if (Array.isArray(countryData?.tld) && countryData.tld.length > 0) {
    topLevelDomain = countryData.tld[0];
  }

  if (typeof countryData?.currencies == "object") {
    let currenciesData = Object.values(countryData.currencies);
    currencies = currenciesData.map((currencyData) => currencyData.name);
  }

  if (typeof countryData?.languages == "object") {
    languages = Object.values(countryData.languages).sort();
  }

  return (
    <>
      {error ? (
        <div>
          <p className="text-lg text-center mt-[30px]">{error.message}</p>
        </div>
      ) : data.length == 0 ? (
        <div className="loading-page">
          <div className="loading-page-container flex justify-center mt-[180px]">
            <img src="./loading.gif" alt="Loading GIF" />
          </div>
        </div>
      ) : (
        <div className="country-page px-4 mt-[30px]">
          <div className="country-page-container max-w-[1200px] mx-auto">
            <div className="back-button-container">
              <Link
                to={"/"}
                className="bg-secondary inline-flex shadow-md px-4 py-2"
              >
                <BackIcon />
                <span className="pl-2">Back</span>
              </Link>
            </div>
            <div className="country-data-container mt-10 lg:flex lg:justify-between">
              <div className="country-flag w-[100%] h-[220px] mb-10 lg:w-[42%] lg:h-[300px]">
                <img
                  className="w-[100%] h-[100%]"
                  src={flagURL}
                  alt={flagAltText}
                />
              </div>
              <div className="country-data lg:w-[50%]">
                <div className="country-data-header mb-8">
                  <h1 className="font-semibold text-2xl lg:text-3xl">
                    {countryName}
                  </h1>
                </div>
                <div className="country-data-body lg:flex">
                  <div className="country-data-section mb-8 lg:w-[50%]">
                    <p className="mb-2">
                      <span className="font-bold">Native Name: </span>
                      <span>{nativeName}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Population: </span>
                      <span>{population}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Region: </span>
                      <span>{region}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Sub Region: </span>
                      <span>{subregion}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Capital: </span>
                      <span>{capital}</span>
                    </p>
                  </div>
                  <div className="country-data-section">
                    <p className="mb-2">
                      <span className="font-bold">Top Level Domain: </span>
                      <span>{topLevelDomain}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Currencies: </span>
                      <span>{currencies?.join(", ")}</span>
                    </p>
                    <p className="mb-2">
                      <span className="font-bold">Languages: </span>
                      <span>{languages?.join(", ")}</span>
                    </p>
                  </div>
                </div>
                <div className="country-data-footer">
                  <p className="font-bold">Border Countries: </p>
                  <div className="border-countries-container flex flex-wrap gap-4 align-middle mt-2">
                    {borderCodes == undefined ? (
                      <p>No border countries are there!</p>
                    ) : (
                      borderCodes?.map((currentBorderCode, index) => {
                        const countryData = data.find(
                          (countryData) => countryData.cca3 == currentBorderCode
                        );
                        const countryName = countryData.name.common;
                        return (
                          <Link
                            key={currentBorderCode + index}
                            to={`/country/${currentBorderCode}`}
                            className="bg-secondary shadow-md px-4 py-2 inline-flex"
                          >
                            {countryName}
                          </Link>
                        );
                      })
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default CountryPage;
